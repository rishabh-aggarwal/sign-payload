const express = require('express')
const app = express();

var NodeRSA = require('node-rsa');
var port = process.env.PORT || 8080; 
var router = express.Router(); 

router.get('/merchantConfig', function(req, res) {
  const fs = require('fs');
  const merchant_id = req.query.merchant_id || "idea_preprod";
  let rawdata = fs.readFileSync('jsonObject.json');
  let result = JSON.parse(rawdata);
  result["merchant_id"] = merchant_id;
  res.json(result);
})

router.get('/:merchant', function(req, res) {
  const fs = require('fs');
  let rawdata = fs.readFileSync('private-key.json');
  let privateKey = JSON.parse(rawdata);
  const merchant = req.params.merchant || "sign-idea";
  const private = privateKey[merchant];
  const encryptKey = new NodeRSA(private);
  var result = encryptKey.sign(req.query.payload,'base64','utf8');
  res.send(result);
});

app.use('/', router);
app.listen(port);
console.log('Magic happens on port ' + port);